# Portfolio

Portfolio est un site internet présentant des peintures

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm

Vous pouvez vérifier les pré-requis (sauf docker et docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
   symfony check:requirements
```
### Lancer l'environnement de développement

```bash
composer install
npm install 
npm run build
docker-compose up -d
symfony serve -d
```
###  Ajouter des données de  tests

```bash
symfony console doctrine:fixtures:load
```
## Lancer des Tests

```bash
php bin/phpunit --testdox 

XDEBUG_MODE=coverage bin/phpunit --coverage-html var/log/test/test-coverage
XDEBUG_MODE=coverage APP_ENV=test symfony php bin/phpunit --coverage-html var/log/test/test-coverage
```
## Production

### Envoie des mails de Contacts
les mails de prise de contact sont stockés en BDD, pour les envoyer au peintre par mail,
il faut mettre en place un cron sur : 
```bash
symfony console app:send-contact
```


```git command
```
git add.

git commit -m  ".....messages..."

git pull

git branch -d < last branch >