<?php

namespace App\EventSubscriber;

use DateTime;
use App\Entity\BlogPost;
use App\Entity\Peinture;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;

class EasyAdminSubscriber implements EventSubscriberInterface
{
    private $slugger;
    private $security;
    public function __construct(SluggerInterface $slugger, Security $security)
    {
        $this->slugger = $slugger;
        $this->security = $security;
    }
    public static function getSubscribedEvents()
    {
        return [
            BeforeEntityPersistedEvent::class => ['setDateAndUser'],
        ];
    }

    public function setDateAndUser(BeforeEntityPersistedEvent $event): void
    {
        $entity = $event->getEntityInstance();

        if (($entity instanceof BlogPost)) {
            $now = new DateTime('now');
            $entity->setCreatedAt($now);

            $author = $this->security->getUser();
            $entity->setUser($author);
        }

        if (($entity instanceof Peinture)) {
            $now = new DateTime('now');
            $entity->setCreatedAt($now);

            $author = $this->security->getUser();
            $entity->setUser($author);
        }
        return;
    }
}
