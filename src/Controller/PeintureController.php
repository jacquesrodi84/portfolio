<?php

namespace App\Controller;

use App\Entity\Peinture;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Services\CommentaireService;
use App\Repository\PeintureRepository;
use App\Repository\CommentaireRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PeintureController extends AbstractController
{
    private $peintureRepository;

    public function __construct(PeintureRepository $peintureRepository)
    {
        $this->peintureRepository = $peintureRepository;
    }

    /**
     * @Route("realisations", name="realisations")
     */
    public function realisations(
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $this->peintureRepository->findBy([], ['id' => 'DESC']);
        $peintures = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            6
        );

        return  $this->render('peinture/realisations.html.twig', ['peintures' => $peintures]);
    }

    /**
     * @Route("/realisations/{slug}", name="realisations_details")
     */
    public function details(
        Peinture $peinture,
        Request $request,
        CommentaireRepository $commentaireRepository,
        CommentaireService $commentaireService
    ): Response {
         $commentaires = $commentaireRepository->findCommentaires($peinture);
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire = $form->getData();
            $commentaireService->persistCommentaire($commentaire, null, $peinture);

            return $this->redirectToRoute('realisations_details', ['slug' => $peinture->getSlug()]);
        }
        return $this->render('peinture/details.html.twig', [
            'peinture'     => $peinture,
            'form'         => $form->createView(),
            'commentaires' => $commentaires
        ]);
    }
}
