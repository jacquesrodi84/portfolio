<?php

namespace App\Tests;

use DateTime;
use App\Entity\BlogPost;
use App\Entity\Peinture;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new BlogPost();
        $peinture = new Peinture();
        $commentaire->setAuteur('auteur')
                    ->setEmail('email@test.com')
                    ->setContenu('contenu')
                    ->setCreatedAt($datetime)
                    ->setBlogpost($blogpost)
                    ->setPeinture($peinture);
        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getPeinture() === $peinture);
    }

    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogpost = new BlogPost();
        $peinture = new Peinture();
        $commentaire->setAuteur('false')
                    ->setEmail('false@test.com')
                    ->setContenu('false')
                    ->setCreatedAt(new DateTime())
                    ->setBlogpost(new BlogPost())
                    ->setPeinture(new Peinture());
        $this->assertFalse($commentaire->getAuteur() === 'auteur');
        $this->assertFalse($commentaire->getEmail() === 'email@test.com');
        $this->assertFalse($commentaire->getContenu() === 'contenu');
        $this->assertFalse($commentaire->getCreatedAt() === $datetime);
        $this->assertFalse($commentaire->getBlogpost() === $blogpost);
        $this->assertFalse($commentaire->getPeinture() === $peinture);
    }

    public function testIsEmpty()
    {
        $commentaire = new Commentaire();
        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getPeinture());
         $this->assertEmpty($commentaire->getId());
    }
}
