<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\PeintureRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PortfolioController extends AbstractController
{
    /**
     * @Route("/portfolio", name="portfolio")
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('portfolio/index.html.twig', [
            'categories' => $categoryRepository->findAll()
        ]);
    }

    /**
     * @Route("/portfolio/{slug}", name="portfolio_category")
     */
    public function categorie(Category $category, PeintureRepository $peintureRepository): Response
    {
        $peintures = $peintureRepository->findAllPortfolio($category);

        return $this->render('portfolio/category.html.twig', [
           'category' => $category,
           'peintures' => $peintures
        ]);
    }
}
