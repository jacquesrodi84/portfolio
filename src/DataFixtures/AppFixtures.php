<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\BlogPost;
use App\Entity\Category;
use App\Entity\Peinture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Mime\Encoder\EncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {

        // Utilisation de faker
        $faker = Factory::create('fr_FR');

        //Création d'un utilisateur
        $user = new User();

        $user->setEmail('user@test.com')
             ->setPrenom($faker->firstName())
             ->setNom($faker->lastName())
             ->setTelephone($faker->phoneNumber())
             ->setAPropos($faker->text())
             ->setInstagram('instagram')
             ->setRoles(['ROLE_PEINTRE']);
        $password = $this->encoder->encodePassword($user, 'password');

        $user->setPassword($password);
        $manager->persist($user);

        for ($i = 0; $i < 10; $i++) {
            $blogpost = new BlogPost();
            $blogpost->setTitre($faker->words(3, true))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setContenu($faker->text(350))
                    ->setSlug($faker->slug(3))
                    ->setUser($user);
            $manager->persist($blogpost);
        }
        // Création d'un Blogpost pour les tests
        $blogpost = new BlogPost();
        $blogpost->setTitre('Blogpost test')
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setContenu($faker->text(350))
                    ->setSlug('blogpost-test')
                    ->setUser($user);
        $manager->persist($blogpost);

        for ($k = 0; $k < 5; $k++) {
            $categorie = new Category();
            $categorie->setNom($faker->word())
                     ->setDescription($faker->words(10, true))
                     ->setSlug($faker->slug());
            $manager->persist($categorie);



            //Création  de 2 peintures / categorie
            for ($j = 0; $j < 2; $j++) {
                $peinture = new Peinture();
                $peinture->setNom($faker->words(3, true))
                     ->setLargeur($faker->randomFloat(2, 20, 60))
                     ->setHauteur($faker->randomFloat(2, 20, 60))
                     ->setEnVente($faker->randomElement([true, false]))
                     ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                     ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                     ->setDescription($faker->text())
                     ->setPortfolio($faker->randomElement([true, false]))
                     ->setSlug($faker->slug())
                     ->setFile('palmier-dessin.jpg')
                     ->addCategory($categorie)
                     ->setPrix($faker->randomFloat(2, 100, 9999))
                     ->setUser($user);
                $manager->persist($peinture);
            }
        }

        // Categorie de test

        $categorie = new Category();
        $categorie->setNom('categorie test')
                  ->setDescription($faker->words(10, true))
                  ->setSlug('categorie-test');
        $manager->persist($categorie);

        // Peinture pour les tests
        $peinture = new Peinture();

        $peinture->setNom('peinture test')
                 ->setLargeur($faker->randomFloat(2, 20, 60))
                 ->setHauteur($faker->randomFloat(2, 20, 60))
                 ->setEnVente($faker->randomElement([true, false]))
                 ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                 ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                 ->setDescription($faker->text())
                 ->setPortFolio($faker->randomElement([true, false]))
                 ->setSlug('peinture-test')
                 ->addCategory($categorie)
                  ->setFile('palmier-dessin.jpg')
                  ->addCategory($categorie)
                  ->setPrix($faker->randomFloat(2, 100, 9999))
                  ->setUser($user);
        $manager->persist($peinture);

        $manager->flush();
    }
}
