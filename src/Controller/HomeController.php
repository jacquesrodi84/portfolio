<?php

namespace App\Controller;

use App\Repository\BlogPostRepository;
use App\Repository\PeintureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $peintureRepository;
    private $blogPostRepository;

    public function __construct(PeintureRepository $peintureRepository, BlogPostRepository $blogPostRepository)
    {
        $this->peintureRepository = $peintureRepository;
        $this->blogPostRepository = $blogPostRepository;
    }
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {

        return $this->render('home/index.html.twig', [
            'peintures' =>  $this->peintureRepository->lastTree(),
            'actualites' => $this->blogPostRepository->getLastThree()
        ]);
    }
}
